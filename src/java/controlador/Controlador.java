/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import UsaServicios.NewWebService_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author david
 */
@WebServlet(name = "Controlador", urlPatterns = {"/Controlador"})
public class Controlador extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/NewWebService/NewWebService.wsdl")
    private NewWebService_Service service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("buscarUsuario");

        if (action != null) {

            String dni = (String) request.getParameter("dni");
            String contrasena = (String) request.getParameter("contrasena");

            boolean existe = identificar(dni, contrasena);
            String existeS = "No existe";
            if (existe) {
                existeS = "Existe";
            }

            HttpSession sesion = request.getSession();
            sesion.setAttribute("existe", existeS);
            sesion.setAttribute("dni", dni);
            sesion.setAttribute("contrasena", contrasena);

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/existeUsuario.jsp");
            rd.forward(request, response);
        }

        action = request.getParameter("gradoUsuario");

        if (action != null) {

            String dni = (String) request.getParameter("dni2");

            String grado = tipoEmpleado(dni);

            HttpSession sesion = request.getSession();
            sesion.setAttribute("grado", grado);
            sesion.setAttribute("dni", dni);

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/gradoUsuario.jsp");
            rd.forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean identificar(java.lang.String dni, java.lang.String password) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        UsaServicios.NewWebService port = service.getNewWebServicePort();
        return port.identificar(dni, password);
    }

    private String tipoEmpleado(java.lang.String dni) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        UsaServicios.NewWebService port = service.getNewWebServicePort();
        return port.tipoEmpleado(dni);
    }

}
